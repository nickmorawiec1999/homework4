# Homework 4 - Test Leaky Stack

Stacks are often used to implement an "undo" feature in image and text editors. However, to save memory, they sometimes have a limited history. For example an editor may only let you undo the last 10 changes you made. As you make more changes, the undo feature forgets the oldest changes. Stacks with this behavior we'll call a leaky stack.

For example, if a leaky stack has a capacity of 3, and we push 1, 2, 3, and 4 (in that order), then the next three pops would return 4, 3, and 2 (in that order) and the stack would be empty.

**Your job is to develop a leaky stack along with a set of automated tests using the methodology taught in class.**

Minimally, your implementation must implement public methods for the following operations.

* push(e) - Add the element e to the top of the stack.
* pop - Remove and return the element on the top of the stack, or raise an exception if the stack is empty.
* isEmpty - True when the stack does not contain any elements.
* size - Return the number of elements in the stack.
* capacity - Return the maximum number of elements in the stack.

Note that each leaky stack has a capacity, and its capacity does not change for the life of the stack.

## Additional requirements

* Place your tests in `/src/test/java/tgjj/TestLeakyStack.java`.
* Place your code in `/src/main/java/tgjj/LeakyStack.java`.
* You code must compile and run without errors under Java 11 using the command: `./gradlew test --info`
* All tests must pass.
* You may not use any existing stack implementations provided by Java or a 3rd party.
* You may use java.util.ArrayList or java.util.LinkedList. Or you may use your own custom implementation using arrays, linked lists, etc.
* Complete the code as the boilerplate indicates by using the methodology taught
  in class.

## Submitting Work (and Getting Started)

1. Fork this project into your individual group within this class on GitLab (e.g., `https://gitlab.com/wne-csit/cs-220/spring-2020/individual/<LASTNAME>`)
2. Remove the fork relationship (see Settings -> General -> Advanced -> Remove Fork Relationship).
3. Clone your project.
4. Stage and commit your work to `master`.
5. Push your changes to your project.

You may stage, commit, and push your changes as you go. I will only grade your
most recent commit at the time I clone your project.

---
Copyright 2020, Brian O'Neill and Stoney Jackson

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
